# Rate Limiting Application with Multiple Algorithms

This project provides a rate limiting application built using Flask, a Python web framework. The application implements various rate limiting algorithms to control and manage the rate of incoming requests from clients. The available rate limiting algorithms include Fixed Window Counter, Sliding Window Counter, Sliding Window Log, and Token Bucket.

#### Challenge: https://codingchallenges.fyi/challenges/challenge-rate-limiter/
## Rate Limiting Algorithms

### Fixed Window Counter (`fixed_window_counter.py`)

- Implements a straightforward fixed window counter rate limiting algorithm.
- Keeps track of the number of requests per IP address within a fixed time window (WINDOW_SIZE).
- If the rate limit is exceeded, it returns a response of "Too many requests."

### Sliding Window Counter (`sliding_window_counter.py`)

- Implements a sliding window counter rate limiting algorithm.
- Uses a sliding window to track the number of requests per IP address.
- Utilizes a weighted count to smooth out the impact of burst traffic.
- Returns a response of "Too many requests" if the rate limit is exceeded.

### Sliding Window Log (`sliding_window_log.py`)

- Implements a sliding window log rate limiting algorithm.
- Logs timestamps for each request and prunes outdated logs to maintain a moving time window.
- Returns a response of "Too many requests" if the rate limit is exceeded.

### Token Bucket (`token_bucket.py`)

- Implements a token bucket rate limiting algorithm.
- Utilizes a token bucket to allow or deny requests based on the availability of tokens.
- Requests consume tokens, and tokens are periodically refilled.
- Returns a response of "Too many requests" if tokens are exhausted.

## Usage

The rate limiting application can be executed by running the `app.py` script. The Flask application initializes and listens on various routes, allowing you to test the rate limiting behavior for different endpoints.

### Available Endpoints

- `/limitedtb`: Token Bucket rate limiting.
- `/limitedfwc`: Fixed Window Counter rate limiting.
- `/limitedswl`: Sliding Window Log rate limiting.
- `/limitedswc`: Sliding Window Counter rate limiting.
- `/unlimited`: Unrestricted access.
-
## TODOs
- [ ] Develop an Application Load Balancer
- [ ] Build a Custom Redis Clone
- [ ] Make Ratelimiter work for multi server setup
