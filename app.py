from flask import Flask, request

from ratelimiters.fixed_window_counter import fixed_window_counter_middleware
from ratelimiters.sliding_window_counter import SlidingWindowCounter
from ratelimiters.sliding_window_log import SlidingLog
from ratelimiters.token_bucket import TokenBucket, ip_buckets

app = Flask(__name__)


@app.route("/limitedtb")
def limitedtb():
    ip = request.headers.get("x-test-ip") or request.remote_addr

    if ip not in ip_buckets:
        ip_buckets[ip] = TokenBucket()

    if ip_buckets[ip].consume():
        return "Limited Access"
    else:
        return "Too many requests", 429


@app.route("/limitedfwc")
def limitedfwc():
    client_ip = request.headers.get("x-test-ip") or request.remote_addr
    result = fixed_window_counter_middleware(client_ip)
    if result is not None:
        return result
    return "Limited Access"


sliding_log = SlidingLog(60, 100)


@app.route("/limitedswl")
def limitedswl():
    # result = sliding_window_log_middleware()
    client_ip = request.headers.get("x-test-ip") or request.remote_addr
    result = sliding_log.limit_request(client_ip)
    if result is not None:
        return result
    return "Limited Access"


sliding_counter = SlidingWindowCounter(60, 100)


@app.route("/limitedswc")
def limitedswc():
    client_ip = request.headers.get("x-test-ip") or request.remote_addr
    result = sliding_counter.limit_request(client_ip)
    if result is not None:
        return result
    return "Limited Access"


@app.route("/unlimited")
def unlimited():
    return "Unlimited Access"


if __name__ == "__main__":
    app.run()
