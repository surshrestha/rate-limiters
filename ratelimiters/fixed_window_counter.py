import time
from collections import defaultdict

rate_limits_info = defaultdict(lambda: {"count": 0, "window_start": 0})

WINDOW_SIZE = 10  # 5 second
MAX_REQUESTS = 5  # max requests per window


def fixed_window_counter_middleware(ip_addr):
    now = time.time()

    limit_info = rate_limits_info[ip_addr]

    if not limit_info:
        rate_limits_info[ip_addr] = {"count": 1, "window_start": now}
        return

    time_elapsed = now - limit_info["window_start"]

    if time_elapsed > WINDOW_SIZE:
        rate_limits_info[ip_addr] = {"count": 1, "window_start": now}
        return

    limit_info["count"] += 1

    if limit_info["count"] > MAX_REQUESTS:
        return "Too many requests", 429
