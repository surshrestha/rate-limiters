import time
from collections import defaultdict


class SlidingWindowCounter:
    def __init__(self, window_size, max_req):
        self.window_size = window_size
        self.max_req = max_req
        self.rate_limit_info = defaultdict(
            lambda: {"curr_win_count": 0, "prev_win_count": 0, "curr_win_start": 0}
        )

    def limit_request(self, ip_addr):
        now = time.time()
        limit_info = self.rate_limit_info[ip_addr]
        curr_win_start = limit_info["curr_win_start"]
        if self._is_window_expired(curr_win_start, now):
            self._update_window_info(limit_info, now)
            return

        self._update_window_info(limit_info, now)
        if self._is_rate_limited(limit_info, now):
            return "Too many requests", 429

    def _is_window_expired(self, current_window_start, now):
        return now - current_window_start >= self.window_size

    def _update_window_info(self, limit_info, now):
        if self._is_window_expired(limit_info["curr_win_start"], now):
            limit_info["prev_win_count"] = limit_info["curr_win_count"]
            limit_info["curr_win_count"] = 1
            limit_info["curr_win_start"] = now
        else:
            limit_info["curr_win_count"] += 1

    def _is_rate_limited(self, limit_info, now):
        # We use a weighted count of the current and previous windows counts to determine the count for the sliding window.
        # This helps smooth out the impact of burst of traffic.
        # For example, if the current window is 40% through, then we weight the previous window’s count by 60% and
        # add that to the current window count.
        weighted_count = (
            1 - (now - limit_info["curr_win_start"]) / self.window_size
        ) * limit_info["prev_win_count"]
        return (limit_info["curr_win_count"] + weighted_count) > self.max_req
