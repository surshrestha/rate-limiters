import time
from collections import defaultdict


class SlidingLog:
    def __init__(self, window_size, max_req):
        self.window_size = window_size
        self.max_req = max_req
        self.log = defaultdict(list)

    def limit_request(self, ip_addr):
        self.add_request(ip_addr)
        if self.is_rate_limited(ip_addr):
            return "Too many requests", 429

    def add_request(self, ip_addr):
        now = time.time()
        self.log[ip_addr].append(now)
        self._prune_outdated_logs(ip_addr, now)

    def _prune_outdated_logs(self, ip_addr, current_time):
        cutoff_time = current_time - self.window_size
        self.log[ip_addr] = [t for t in self.log[ip_addr] if t >= cutoff_time]

    def get_request_count(self, ip_addr):
        return len(self.log[ip_addr])

    def is_rate_limited(self, ip_addr):
        now = time.time()
        self._prune_outdated_logs(ip_addr, now)
        return self.get_request_count(ip_addr) > self.max_req


# req_logs = defaultdict(list)
# def sliding_window_log_middleware():
#     client_ip = request.headers.get("x-test-ip") or request.remote_addr
#     now = time.time()
#     req_logs[client_ip].append(now)
#
#     req_logs[client_ip] = [t for t in req_logs[client_ip] if now - t <= WINDOW_SIZE]
#     # print(req_logs[client_ip])
#
#     req_rate = len(req_logs[client_ip])
#     print(req_rate)
#     if req_rate > MAX_REQUESTS:
#         return "Too many requests", 429
