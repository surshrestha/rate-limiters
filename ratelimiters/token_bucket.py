import threading
import time

ip_buckets = {}


# Refill tokens every second
def refill_tokens_interval():
    while True:
        time.sleep(1)
        for bucket in ip_buckets.values():
            bucket.refill()


# Start the token refill process in the background
refill_thread = threading.Thread(target=refill_tokens_interval)
refill_thread.daemon = True
refill_thread.start()


class TokenBucket:
    def __init__(self):
        self.tokens = 10
        self.last_filled = time.time()

    def refill(self):
        if self.tokens == 10:
            return
        now = time.time()
        time_passed = now - self.last_filled
        if time_passed >= 1:
            self.tokens = min(self.tokens + 1, 10)
            self.last_filled = now

    def consume(self):
        if self.tokens < 1:
            return False
        self.tokens -= 1
        return True
