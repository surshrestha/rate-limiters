import unittest

from ratelimiters.fixed_window_counter import fixed_window_counter_middleware


class TestFixedWindowCounter(unittest.TestCase):
    def test_single_request_within_window(self):
        result = fixed_window_counter_middleware("127.0.0.1")
        self.assertIsNone(result)

    def test_requests_exceeding_limit(self):
        for _ in range(10):
            fixed_window_counter_middleware("127.0.0.1")
        result = fixed_window_counter_middleware("127.0.0.1")
        self.assertEqual(result, ("Too many requests", 429))


if __name__ == "__main__":
    unittest.main()
