import unittest

from ratelimiters.sliding_window_counter import SlidingWindowCounter


class TestSlidingWindowCounter(unittest.TestCase):
    def test_requests_within_window(self):
        sliding_counter = SlidingWindowCounter(60, 10)
        for _ in range(5):
            result = sliding_counter.limit_request("127.0.0.1")
            self.assertIsNone(result)

    def test_requests_exceeding_limit(self):
        sliding_counter = SlidingWindowCounter(60, 10)
        for _ in range(12):
            sliding_counter.limit_request("127.0.0.1")
        result = sliding_counter.limit_request("127.0.0.1")
        self.assertEqual(result, ("Too many requests", 429))


if __name__ == "__main__":
    unittest.main()
