import time
import unittest

from ratelimiters.sliding_window_log import SlidingLog


class TestSlidingLog(unittest.TestCase):
    def test_add_request_to_sliding_log(self):
        sliding_log = SlidingLog(60, 10)
        sliding_log.add_request("127.0.0.1")
        self.assertEqual(sliding_log.get_request_count("127.0.0.1"), 1)

    def test_prune_outdated_log(self):
        sliding_log = SlidingLog(60, 10)
        sliding_log.add_request("127.0.0.1")
        sliding_log.add_request("127.0.0.1")
        # sleep to simulate time passing
        time.sleep(61)
        sliding_log.add_request("127.0.0.1")
        self.assertEqual(sliding_log.get_request_count("127.0.0.1"), 2)

    def test_req_count_within_sliding_window(self):
        sliding_log = SlidingLog(60, 10)
        sliding_log.add_request("127.0.0.1")
        sliding_log.add_request("127.0.0.1")
        # sleep to simulate time passing
        time.sleep(30)
        sliding_log.add_request("127.0.0.1")
        self.assertEqual(sliding_log.get_request_count("127.0.0.1"), 3)

    def test_single_request_within_window(self):
        sliding_log = SlidingLog(60, 10)
        result = sliding_log.limit_request("127.0.0.1")
        self.assertIsNone(result)

    def test_requests_exceeding_limit(self):
        sliding_log = SlidingLog(60, 10)
        for _ in range(12):
            sliding_log.limit_request("127.0.0.1")
        result = sliding_log.limit_request("127.0.0.1")
        self.assertEqual(result, ("Too many requests", 429))


if __name__ == "__main__":
    unittest.main()
