import unittest

from ratelimiters.token_bucket import TokenBucket


class TestTokenBucket(unittest.TestCase):
    def test_requests_within_limit(self):
        token_bucket = TokenBucket()
        for _ in range(5):
            result = token_bucket.consume()
            self.assertTrue(result)

    def test_requests_exceeding_limit(self):
        token_bucket = TokenBucket()
        for _ in range(12):
            token_bucket.consume()
        result = token_bucket.consume()
        self.assertFalse(result)


if __name__ == "__main__":
    unittest.main()
